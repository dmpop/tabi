# Tabi

Tabi super simple web-based application for viewing and GPX tracks on OpenStreetMap. The application relies on [GPX Viewer](https://www.j-berkemeier.de/GPXViewer/) JavaScript library.

## Dependencies

- PHP
- Web server (Apache, Lighttpd, or similar)
- Git (optional)

## Installation and Usage

1. Make sure that your local machine or remote web server has PHP installed.
2. Clone the project's repository using the `git clone https://gitlab.com/dmpop/tabi.git` command. Alternatively, download the latest source code using the appropriate button on the project's page.
3. Open the _tabi/config.php_ file and change the example value of the `passwd` variable.
4. Upload the _tabi_ directory to the document root of your web server.

## Problems?

Please report bugs and issues in the [Issues](https://gitlab.com/dmpop/tabi/issues) section.

## Contribute

If you've found a bug or have a suggestion for improvement, open an issue in the [Issues](https://gitlab.com/dmpop/tabi/issues) section.

To add a new feature or fix issues yourself, follow the following steps.

1. Fork the project's repository
2. Create a feature branch using the `git checkout -b new-feature` command
3. Add your new feature or fix bugs and run the `git commit -am 'Add a new feature'` command to commit changes
4. Push changes using the `git push origin new-feature` command
5. Submit a merge request

## Author

[Dmitri Popov](https://tokyoma.de)

# License

The [GNU General Public License version 3](http://www.gnu.org/licenses/gpl-3.0.en.html)
