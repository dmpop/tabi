<?php
include('config.php');
if ($protect) {
    require_once('protect.php');
}
?>

<!DOCTYPE html>
<html lang="en">
    <!-- Author: Dmitri Popov, dmpop@linux.com
         License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt -->
    <head>
	<meta charset="utf-8">
	<title>Tabi</title>
	<link rel="icon"
		href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>🚅</text></svg>">
	<link rel="stylesheet" href="lit.css">
	<link href="https://fonts.googleapis.com/css2?family=Nunito" rel="stylesheet">
	<script type="text/javascript" src="GM_Utils/GPX2GM.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
	<div class="c">
	    <?php
		if (!file_exists($gpx_dir)) {
			mkdir($gpx_dir, 0777, true);
			}
	    $files = glob($gpx_dir."/*.gpx");
	    $list=implode(",", $files);
		echo "<h1>Tabi</h1>";
		echo "<hr style='margin-top:2em'>";
	    echo "<div id='map' class='gpxview:$list' style='width:100%;height:600px;margin-top:2em'></div>";
		echo "<noscript><p>Enable JavaScript</p></noscript>";
	    ?>
		<script>
		var Bestaetigung = false;
		var Legende_fnm = false;
		var Fullscreenbutton = true;
		 </script>
		<!-- <button type="button" style='margin-top:2em' class="gpxview:map:skaliere">Reset</button> -->
		<?php
		if (isset($_POST['upload'])) {
			// count total files
			$countfiles = count($_FILES['file']['name']);
			// looping all files
			for ($i = 0; $i < $countfiles; $i++) {
				$filename = $_FILES['file']['name'][$i];
				if (!file_exists($gpx_dir)) {
					mkdir($gpx_dir, 0777, true);
				}
				// upload file
				move_uploaded_file($_FILES['file']['tmp_name'][$i], $gpx_dir . DIRECTORY_SEPARATOR . $filename);
			}
			echo '<meta http-equiv="refresh" content="0">';
		}
		if (isset($_POST['delete'])) {
			array_map( 'unlink', array_filter((array) glob($gpx_dir."/*") ) );
			echo '<meta http-equiv="refresh" content="0">';
		}
		?>
		<form method='post' action='' enctype='multipart/form-data'>
			<input type="file" name="file[]" id="file" multiple>
			<p></p>
			<button class="btn primary" type="submit" role="button" name="upload">Upload</button>
			<button class="btn" type="submit" role="button" name="delete">Delete all</button>
		</form>
	<hr style='margin-top:2em'>
	This is <a href="https://gitlab.com/dmpop/tabi">Tabi</a>
	</div>
    </body>
</html>
